/**
syntaxe:
describe('Abre página de login', function() {
  assertions...
})
***/
describe('Abre página de login', function() {
  it('Deve abrir página de login', function() {
    cy.visit('/login')
  });

  it('Deve conter labels de login', function() {
    cy.contains('Login')
    cy.contains('Nome de usuário')
    cy.contains('Senha')
    cy.contains('Entrar')
  });

  it('Deve conter campos de login', function() {
    cy.get('input[name="login"]')
    cy.get('input[name="senha"]')
    cy.get('#btn_login')
  });
});

describe('Dado que página de login está aberta', function() {
  it('Então deve mostrar mensagem de erro com credenciais inválidas', function() {
    cy.get('input[name="login"]').type('usuário inválido')
    cy.get('input[name="senha"]').type('senha inválida')
    cy.get('#btn_login').click()
    cy.contains('Acesso negado. Erro na credencial de acesso.')
  });

  it('Ou se usuário inválido deve mostrar mensagem de erro', function() {
    cy.get('input[name="login"]').type('usuário inválido')
    cy.get('input[name="senha"]').type(Cypress.env('userpass'))
    cy.get('#btn_login').click()
    cy.contains('Acesso negado. Erro na credencial de acesso.')
  });

  it('Ou se senha inválida deve mostrar mensagem de erro', function() {
    cy.get('input[name="login"]').type(Cypress.env('username'))
    cy.get('input[name="senha"]').type('senha inválida')
    cy.get('#btn_login').click()
    cy.contains('Acesso negado. Erro na credencial de acesso.')
  });

});
