/**
syntaxe:
describe('Abre página de login', function() {
  assertions...
})
***/
describe('Abre página de login', function() {
  it('Deve abrir página de login', function() {
    cy.visit('/login')
  });

  it('Deve conter labels de login', function() {
    cy.contains('Login')
    cy.contains('Nome de usuário')
    cy.contains('Senha')
    cy.contains('Entrar')
  });

  it('Deve conter campos de login', function() {
    cy.get('input[name="login"]')
    cy.get('input[name="senha"]')
    cy.get('#btn_login')
  });
});

describe('Dado que página de login está aberta', function() {
  it('Então deve realizar login com credenciais válidas', function() {
    cy.get('input[name="login"]').type(Cypress.env('username'))
    cy.get('input[name="senha"]').type(Cypress.env('userpass'))
    cy.get('#btn_login').click()
    cy.contains(Cypress.env('username'))
    cy.contains('Sair')
  });
});
