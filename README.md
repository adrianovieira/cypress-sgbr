# SGBR - Casos de testes

Projeto de referência de implementação de testes BDD.

Os casos de testes aqui são implementados usado o *[Cypress](http://cypress.io)*.

## Tecnologias usadas

- Cypress-3.4

## Instalação

A instalação do *Cypress* pode ser realizada via *nodeJs npm* ou via download para a sua plataforma.

Execute a instalação seguindo a documentação dispinível em https://docs.cypress.io/guides/getting-started/installing-cypress.html

## Execução dos testes

Casos de Testes implementados neste projeto de referência:

- **Login válido**: teste com credenciais de usuário e senha válidos.
  - *Login*: teste com credenciais válidas.
  - *Login inválido*: testes com creadenciais inválidas.

A execução dos testes é realizada via o comando `cypress`. Abaixo um exemplo de execução:

```bash
cypress run --env username=$username --env userpass=$userpass
```

Onde:

- `username`: é um usuário válido para acesso
- `userpass`: é uma senha válida para acesso

#### Resultado do teste

Cópia de tela dos resultados da execusão via *UI* do Cypress.

1. Relatórios gerados:
    ![Login Report](docs/imagens/cypress-logging.png)
